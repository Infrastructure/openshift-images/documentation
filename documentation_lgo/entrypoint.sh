#!/bin/bash

set -x

mkdir -p /data/logs

DATE="$(date +%s)"

#/usr/local/bin/release-notes-gen |& tee "/data/logs/relnotes-$DATE.log"
python /opt/lgo/src/lgo.py -vvvvv -c /opt/library-web.lgorc |& tee "/data/logs/library-$DATE.log"

cd /data/output/help.gnome.org/misc/release-notes || exit
REPO="https://gitlab.gnome.org/Teams/Engagement/release-notes.git"
LATEST_RELEASE="$(git ls-remote --heads ${REPO} | awk '{print $2}' | cut -d/ -f3 | grep '^gnome-' | sort -V | tail -n1 | cut -d- -f2- | sed 's/-/./')"
if [[ $LATEST_RELEASE -ge 40 ]]; then
  LATEST_RELEASE="${LATEST_RELEASE}.0"
fi

rm -f stable
[[ -d "${LATEST_RELEASE}" ]] && ln -sf "${LATEST_RELEASE}" stable

cd /data/logs || exit
zstd -f --rm ./*.log
